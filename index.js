const express = require('express');

const app = express();

const port = 3000;

app.use(express.json());

app.use(express.urlencoded({extended: true}));

app.listen(port, () => console.log(`Server is running at http://localhost:${port}`));

app.get("/home", (request, response) => {
	response.send("Welcome to the home page!");
});

let users=[];

app.post("/register", (request, response) => {
	if(request.body.username == "" || request.body.password == "")
	{
		response.send("Username and password cannot be empty");
	}
	else
	{
		users.push(request.body);
		response.send(`User ${request.body.username} successfully registered!`);
	}
})

app.get("/users", (request,response) => {
	response.send(users);
})

app.delete("/delete-user", (request,response) => {
	let message;
	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users.splice(i, 1);
			message = `User ${request.body.username} has been deleted successfully!`
			// message = "User exist";
			break;
		}
		else
		{
			message = "User does not exist!"
		} 
	}
	response.send(message);
})